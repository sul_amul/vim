"------------------------------------------------------------------------
" Basic options
"------------------------------------------------------------------------

syntax off
set tabstop=4
set noet sts=0 sw=4 ts=4
set cindent
set cinoptions=(0,u0,U0
set nowrap
set textwidth=80
set modelines=0
set autoindent
set showcmd
set hidden
"set visualbell
set ruler
set backspace=indent,eol,start
set laststatus=2
set history=1000
set undofile
" set list
set listchars=tab:▸\ ,eol:¬,extends:❯,precedes:❮
set lazyredraw
"set showbreak=↪
set splitbelow
set splitright
set autowrite
set autoread
set shiftround
set title
set linebreak
set rnu
set mouse=r
set formatoptions=croql
set cscopequickfix=s-,c-,d-,i-,t-,e-

filetype indent plugin on
au FileType gitcommit setlocal tw=70

"------------------------------------------------------------------------
" Wildmenu completion
"------------------------------------------------------------------------

set wildmenu
"set wildmode=list:longest
set wildignore+=.git
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.spl                            " compiled spelling word lists
set wildignore+=*.sw?                            " Vim swap files

"------------------------------------------------------------------------
" Backups
"------------------------------------------------------------------------

set undodir=/tmp/vim_undodir
set backupdir=/tmp/vim_backup
set backup                        " enable backups

if !isdirectory("/tmp/vim_undodir")
     call mkdir("/tmp/vim_undodir", "", 0700)
endif
if !isdirectory("/tmp/vim_backup")
     call mkdir("/tmp/vim_backup", "", 0700)
endif

function! <SID>ForgetUndo()
		let old_undolevels = &undolevels
		set undolevels=-1
		exe "normal a \<BS>\<Esc>"
		let &undolevels = old_undolevels
		unlet old_undolevels
endfunction
command -nargs=0 ClearUndo call <SID>ForgetUndo()

"------------------------------------------------------------------------
" Convenience mapping
"------------------------------------------------------------------------

" Vert terminal launch
nnoremap <leader>t :vert term<CR>
nnoremap <leader>ts :term<CR>

" TAB to find matching ([{}])
nnoremap <tab> %

" Save
nnoremap s :w<cr>

vnoremap u :<C-u>echo "ERROR : case change key is disabled !"<CR>

" Wrap
nnoremap <leader>W :set wrap!<cr>

highlight matchRed ctermbg=red ctermfg=white guibg=#592929
" :WS command will match whiteSpaces
command WS match matchRed /\s\+$/
" :OL command will match overlength string
command OL match matchRed /\%81v.\+/

" Substitute
"nnoremap <c-s> :%s/
"vnoremap <c-s> :s/

" Clean trailing whitespace
nnoremap <leader>w mz:%s/\s\+$//<cr>:let @/=''<cr>`z

" Show functions in current file.
nnoremap <silent> <leader>l :TlistToggle<CR>

"------------------------------------------------------------------------
" Searching and movement
"------------------------------------------------------------------------

" Use sane regexes.
nnoremap / /\v
vnoremap / /\v

set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch
" set gdefault

set scrolloff=5
set sidescroll=1
set sidescrolloff=10

set virtualedit+=block

noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

" Keep search matches in the middle of the window.
nnoremap n nzzzv
nnoremap N Nzzzv

" Don't move on *
noremap * *<C-O>

" Open a Quickfix window for the last search.
nnoremap <silent> <leader>? :execute 'vimgrep /'.@/.'/g %'<CR>:botright copen<CR>


" Ack for the last search.
nnoremap <silent> <leader>/ :execute "Ack! '" . substitute(substitute(substitute(@/, "\\\\<", "\\\\b", ""), "\\\\>", "\\\\b", ""), "\\\\v", "", "") . "'"<CR>

"------------------------------------------------------------------------
" Highlight word
"------------------------------------------------------------------------
highlight InterestingWord1 ctermbg=red ctermfg=white guibg=#592929
highlight InterestingWord2 ctermbg=Blue ctermfg=white guibg=#0000ff
highlight InterestingWord3 ctermbg=green ctermfg=white guibg=#00ff00
nnoremap <silent> <leader>hh :execute 'match InterestingWord1 /\<<c-r><c-w>\>/'<cr>
nnoremap <silent> <leader>h1 :execute 'match InterestingWord1 /\<<c-r><c-w>\>/'<cr>
nnoremap <silent> <leader>h2 :execute '2match InterestingWord2 /\<<c-r><c-w>\>/'<cr>
nnoremap <silent> <leader>h3 :execute '3match InterestingWord3 /\<<c-r><c-w>\>/'<cr>

"------------------------------------------------------------------------
" Visual Mode */# from Scrooloose
"------------------------------------------------------------------------

function! s:VSetSearch()
  let temp = @@
  norm! gvy
  let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
  let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR><c-o>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR><c-o>


"------------------------------------------------------------------------
" Split window nevigation
"------------------------------------------------------------------------
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Auto ajust split window size
" set winwidth=84
" set winheight=5
" set winminheight=5
" set winheight=999

"------------------------------------------------------------------------
" Make sure Vim returns to the same line when you reopen a file.
"------------------------------------------------------------------------
augroup line_return
    au!
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \     execute 'normal! g`"zvzz' |
        \ endif
augroup END

"------------------------------------------------------------------------
" Folding
"------------------------------------------------------------------------
set foldlevelstart=0

" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za
" Load only C files
:au BufRead,BufNewFile *.c,*.h,*.y,*.l set foldmethod=syntax
set foldlevelstart=99


"------------------------------------------------------------------------
" Cscope
"------------------------------------------------------------------------
"autocomplete module
"map <F4> :!ctags -R --sort=yes --fields=+iaS --extra=+q .<CR>

" map <F12> :!cscope -b<CR>:cs reset<CR><CR>
nmap <F5> :!find $PWD -iname '*.c' -a -not -iname '*gram.c' -o -iname '*.h' -o -iname '*.y' -o -iname '*.l' > cscope.files<CR>
	\:!cscope -b -i cscope.files -f cscope.out<CR>
	\:cs reset<CR>
	\:!ctags -L cscope.files --sort=yes --fields=+iaS --extra=+q .<CR>

cs add cscope.out

" This script adds keymap macro as follows:
" <C-\> s: Find this C symbol
" <C-\> g: Find this definition
" <C-\> d: Find functions called by this function
" <C-\> c: Find functions calling this function
" <C-\> t: Find this text string
" <C-\> e: Find this egrep pattern
" <C-\> f: Find this file
" <C-\> i: Find files #including this file
nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR><C-o>:botright copen<CR>
nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR><C-o>:botright copen<CR>
nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR><C-o>:botright copen<CR>

comm! -nargs=1 S :cs find s <args>
comm! -nargs=1 C :cs find c <args>
comm! -nargs=1 T :cs find t <args>
comm! -nargs=1 E :cs find e <args>
comm! -nargs=1 F :cs find f <args>
comm! -nargs=1 G :cs find g <args>

noremap <F10> :botright copen<cr>
" Jump to previous in quickfix list
nnoremap <C-p> :cp<CR>
" Jump to next in quickfix list
nnoremap <C-n> :cn<CR>

" Jump window open
nnoremap <C-j> :jumps<CR>
" Marks window open
nnoremap <C-m> :marks<CR>
"------------------------------------------------------------------------
" Hard time pluging
"------------------------------------------------------------------------
"let g:hardtime_default_on = 1 "set this in local vimrc
nnoremap <leader>h <Esc>:call HardTimeToggle()<CR>

"------------------------------------------------------------------------
" Statusline: need fugitive.vim plugin
"------------------------------------------------------------------------
function! StatuslineMode()
  let l:mode=mode()
  if l:mode==#"n"
    return "NORMAL"
  elseif l:mode==?"v"
    return "VISUAL"
  elseif l:mode==#"i"
    return "INSERT"
  elseif l:mode==#"R"
    return "REPLACE"
  endif
endfunction

set statusline=
set statusline+=%{g:currentmode[mode()]}

set laststatus=2
"set noshowmode
set statusline=
set statusline+=%0*\ %n\                                 " Buffer number
set statusline+=%1*\ %<%f%m%r%h%w\                       " File path, modified, readonly, helpfile, preview
set statusline+=%2*|                                     " Separator
"set statusline+=%3{StatuslineMode()}                         " Mode
set statusline+=%=                                       " Right Side
set statusline+=%2*|                                     " Separator
set statusline+=%2*\ col:\ %02v\                         " Colomn number
set statusline+=%2*|                                     " Separator
set statusline+=%1*\ ln:\ %02l\ (%3p%%)\              " Line number / total lines, percentage of document
set statusline+=%2*|                                     " Separator
set statusline+=%{FugitiveStatusline()}

hi User1 ctermfg=007 ctermbg=239 guibg=#4e4e4e guifg=#adadad
hi User2 ctermfg=007 ctermbg=236 guibg=#303030 guifg=#adadad
hi User3 ctermfg=236 ctermbg=236 guibg=#303030 guifg=#303030
hi User4 ctermfg=239 ctermbg=239 guibg=#4e4e4e guifg=#4e4e4e

"------------------------------------------------------------------------
" vimdiff colorscheme
"------------------------------------------------------------------------
highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red
if &diff
    syntax off
endif

"------------------------------------------------------------------------
" NOTES
"------------------------------------------------------------------------
"
"1. Insert current file with relative path press "%p
"
